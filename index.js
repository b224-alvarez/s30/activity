// [SECTION] MongoDB Aggregation Method
// Used to generate manipulated data and perform operations tp create filtered results that helps in analayzing data

db.fruits.insertMany([
  {
    name: "Apple",
    color: "Red",
    stock: 20,
    price: 40,
    supplier_id: 1,
    onSale: true,
    origin: ["Philippines", "US"],
  },
  {
    name: "Banana",
    color: "Yellow",
    stock: 15,
    price: 20,
    supplier_id: 2,
    onSale: true,
    origin: ["Philippines", "Ecuador"],
  },
  {
    name: "Kiwi",
    color: "Green",
    stock: 25,
    price: 50,
    supplier_id: 1,
    onSale: true,
    origin: ["US", "China"],
  },
  {
    name: "Mango",
    color: "Yellow",
    stock: 10,
    price: 120,
    supplier_id: 2,
    onSale: false,
    origin: ["Philippines", "India"],
  },
]);

// Single-Purpose Aggregation Operations
// If we only need simple aggregations with only 1 stage
db.fruits.count();

// Pipelines with multiple stages
db.fruits.aggregate([
  {
    $match: { onSale: true }, //stage 1
  },
  {
    $group: { _id: "$supplier_id", tools: { $sum: "$stock" } }, //stage 2
  },
]);

// MINI ACTIVITY

db.fruits.aggregate([
  {
    $match: { stock: { $gte: 20 } },
  },
  {
    $group: { _id: "supplier_id", tools: { $sum: "$stock" } },
  },
]);

// Field Projection with aggregation
// The $project can be used when aggregating data to include/exclude fields from the returned results
/* 
  SYNTAX:
      {$project: {field: 1/0}}
  */
db.fruits.aggregate([
  {
    $match: { onSale: true }, //stage 1
  },
  {
    $group: { _id: "$supplier_id", tools: { $sum: "$stock" } }, //stage 2
  },
  {
    $project: { _id: 0 },
  },
]);

// Sorting aggregating result
// The "$sort" can be used to change the order of aggregrated resutls
// "1" - sort ascending
// "-1" - sort descending
db.fruits.aggregate([
  {
    $match: { onSale: true }, //stage 1
  },
  {
    $group: { _id: "$supplier_id", tools: { $sum: "$stock" } }, //stage 2
  },
  {
    $sort: { total: 1 },
  },
]);

// Aggregrating results based on array field
db.fruits.aggregate([
  {
    $unwind: "$origin",
  },
]);

// Display fruit documents by their origin and the number of kinds of fruits that they are supplying
db.fruits.aggregate([
  {
    $unwind: "$origin",
  },
  {
    $group: { _id: "origin", kinds: { $sum: 1 } },
  },
]);

// 1.
db.fruits.aggregate([
  {
    $match: { onSale: true },
  },
  {
    $group: { _id: "$onSale", fruitsOnSale: { $count: {} } },
  },
  {
    $project: { _id: 0 },
  },
]);

// 2.
db.fruits.aggregate([
  {
    $match: { stock: { $gt: 20 } },
  },
  {
    $group: { _id: "$stock", enoughStock: { $count: {} } },
  },
  {
    $project: { _id: 0 },
  },
]);

// 3.
db.fruits.aggregate([
  {
    $match: { onSale: true },
  },
  {
    $group: { _id: "$supplier_id", avg_price: { $avg: "$price" } },
  },
]);

//4.
db.fruits.aggregate([
  {
    $match: { onSale: true },
  },
  {
    $group: { _id: "$supplier_id", max_price: { $max: "$price" } },
  },
]);

//5.
db.fruits.aggregate([
  {
    $match: { onSale: true },
  },
  {
    $group: { _id: "$supplier_id", min_price: { $min: "$price" } },
  },
]);
